#!/usr/bin/env python3

import sys
from telegram.ext import Updater
from subprocess import check_output
from telegram.ext import Filters, MessageHandler
import logging

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)


def start(bot, update):
    # подробнее об объекте update: https://core.telegram.org/bots/api#update
    yodaism = check_output(['yodaism']).decode('ascii')
    msg_from = update.message.from_user.first_name
    logging.info('получено сообщение от пользователя ' + str(msg_from))
    bot.sendMessage(chat_id=update.message.chat_id, text=yodaism)


updater = Updater(token=sys.argv[1])  # тут токен, который выдал вам Ботский Отец!

text_handler = MessageHandler(Filters.text, start)
command_handler = MessageHandler(Filters.command, start)
# регистрируем свеженькие обработчики в диспетчере
updater.dispatcher.add_handler(text_handler)     # без регистрации будет работать,
updater.dispatcher.add_handler(command_handler)  # но не больше трёх месяцев (шутка)

updater.start_polling()
logging.info('получаем любое сообщение, в ответ отправляем йодаизм.')
